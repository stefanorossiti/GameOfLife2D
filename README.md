# Conway's Game Of Life 2D

- Fully working [GOL](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) 
Simulator
- Randomly and manually fill cell
- Play/Pause the simulation
- Color based on future cell state
- Draw cells with mouse anytime

![ezgif-1-39fe231485.gif](images/ezgif-1-39fe231485.gif)

## Description

You will start in a premade map.

![Scheme](images/start.png)

Just press play and enjoy.

![Scheme](images/started.png)

You can fill with random cells

![Scheme](images/filled.png)

Or just draw them: Left mouse click to add cells. Right to remove.

Cell color is based on their future stare. Red = dead, green = alive.

## Requirements

- Java (6 to) 8 (max, recommended)
- NetBeans (highly recommended)

Java Applets have been dropped with Java 9. So you need an older versione to run this project.
Java 8 is the preferred version. Until Java 6 should be ok.

Problems running the project are expected, applets are deprecated and not supported by modern browsers.**

This is a NetBeans Project. Using NetBeans isn't mandatory but recommended for the built-in applet runner.
You can run the GrigliaApplet.java class with netbeans.

[NetBeans Instructions](https://netbeans.apache.org/tutorial/main/kb/docs/java/javase-jdk8/)

Alternatively, you can use IntellijJ IDEA with the [Applet runner plugin](https://plugins.jetbrains.com/plugin/16682-applet-runner) (not tested).

## Installation and run

Clone the repo:

```bash
git clone https://gitlab.com/stefanorossiti/GameOfLife2D.git
```

Run `GameOfLifeApplet.java` with your preferred IDE (NetBeans is recommended) 
using java 8.

## License

All my work is released under [DBAD](https://www.dbad-license.org/) license.